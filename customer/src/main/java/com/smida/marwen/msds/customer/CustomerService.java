package com.smida.marwen.msds.customer;

import org.springframework.stereotype.Service;

@Service

public class CustomerService {
    final CustomerRepo customerRepo;

    public CustomerService(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    public void register(CustomerRegistrationRequest request) {
        Customer customer =  Customer.builder()
                        .firstName(request.fisrtName())
                        .lastName(request.lastName())
                        .email(request.email())
                        .build();

        customerRepo.save(customer);
        // todo : validate the email
        // todo : check if the email is valid


    }
}
